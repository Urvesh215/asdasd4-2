function startTime() {
	var today = new Date();
	var h = today.getHours();
	var m = today.getMinutes();
	var s = today.getSeconds();
	m = checkTime(m);
	s = checkTime(s);
	document.getElementById('clock').innerHTML = 
	h + ":" + m + ":" + s;
	var t = setTimeout(startTime, 500);
}
function checkTime (i) {
	if (i < 10) {i = "0" + i};
	return i;
}

var d = new Date();
document.getElementById('date').innerHTML = d.toDateString();

function swap(arra) {
    [arra[0], arra[arra.length - 1]] = [arra[arra.length - 1], arra[0]];
    return arra;
}
console.log(swap([1, 2, 3, 4]));  
console.log(swap([0, 2, 1]));  
console.log(swap([3])); 

function scroll() {
  $('#scrolltext').css('left', $('#scrollcontainer').width());
  $('#scrolltext').animate({
    left: '-='+($('#scrollcontainer').width()+$('#scrolltext').width())
  }, 20000, function() {
    scroll();
  });
}

scroll();

(function() {
  
  $(".sidebar-sub-toggle").on("show.bs.collapse hide.bs.collapse", function(e) {
    if (e.type=='show'){
      $(this).addClass('active');
    }else{
      $(this).removeClass('active');
    }
  });  

}).call(this);
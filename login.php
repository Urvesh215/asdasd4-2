<?php

session_start();
$_SESSION['logged_in'] = 'false';
require_once "api_info.php";

if($_SERVER["REQUEST_METHOD"] == "POST") {

    $username = $_POST['username'];
    $password = $_POST['password'];
    $data = array("username" => $username, "password" => $password);
    $data_string = json_encode($data);

    $ch = curl_init($api_server."login");

    set_curl_opts_post($ch, $data_string);

    $result = json_decode(curl_exec($ch));
    if($result->{'IsSucess'} == '1')
    {
        $_SESSION["username"] = $username;
        $_SESSION["acct_level"] = $result->{'AcctLevel'};
        $_SESSION["logged_in"] = 'true';
        $_SESSION["parent"] = $result->{'Parent'};
        $_SESSION["acct_type"] = $result->{'AcctType'};
        header("location:index.php");
    }

    else {
        echo "<script type='text/javascript'>alert('Incorrect login!');</script>";


    }


}


?>

<!DOCTYPE html>
<html>
<head>
    <link rel="shortcut icon" href="adm/assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="adm/assets/img/favicon.ico" type="image/x-icon">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Bwyn123</title>

</head>

<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="adm/assets/css/bootstrap.min.css">
<style type="text/css">

    * { box-sizing: border-box; padding:0; margin: 0; }

    body {
        font-family: "HelveticaNeue-Light","Helvetica Neue Light","Helvetica Neue",Helvetica,Arial,"Lucida Grande",sans-serif;
        color:white;
        font-size:12px;
    }

    form {
        background:#111;
        width:300px;
        margin: 150px auto;
        border-radius:0.4em;
        border:1px solid #191919;
        overflow:hidden;
        position:relative;
        box-shadow: 0 5px 10px 5px rgba(0,0,0,0.2);
    }

    form:after {
        content:"";
        display:block;
        position:absolute;
        height:1px;
        width:100px;
        left:20%;
        background:linear-gradient(left, #111, #444, #b6b6b8, #444, #111);
        top:0;
    }

    form:before {
        content:"";
        display:block;
        position:absolute;
        width:8px;
        height:5px;
        border-radius:50%;
        left:34%;
        top:-7px;
        box-shadow: 0 0 6px 4px #fff;
    }

    .inset {
        padding:20px;
        border-top:1px solid #19191a;
    }

    form h1 {
        font-size:18px;
        text-shadow:0 1px 0 black;
        text-align:center;
        padding:15px 0;
        border-bottom:1px solid rgba(0,0,0,1);
        position:relative;
    }

    form h1:after {
        content:"";
        display:block;
        width:250px;
        height:100px;
        position:absolute;
        top:0;
        left:50px;
        pointer-events:none;
        transform:rotate(70deg);
        background:linear-gradient(50deg, rgba(255,255,255,0.15), rgba(0,0,0,0));

    }

    label {
        color:#666;
        display:block;
        padding-bottom:9px;
    }

    input[type=text],
    input[type=password] {
        width:100%;
        padding:8px 5px;
        background:linear-gradient(#1f2124, #27292c);
        border:1px solid #222;
        box-shadow:
                0 1px 0 rgba(255,255,255,0.1);
        border-radius:0.3em;
        margin-bottom:20px;
        color:whitesmoke;

    }

    label[for=remember]{
        color:white;
        display:inline-block;
        padding-bottom:0;
        padding-top:5px;
    }

    input[type=checkbox] {
        display:inline-block;
        vertical-align:top;
    }

    .p-container {
        padding:0 20px 20px 20px;
    }

    .p-container:after {
        clear:both;
        display:table;
        content:"";
    }

    .p-container span {
        display:block;
        float:left;
        color:#0d93ff;
        padding-top:8px;
    }

    input[type=submit] {
        padding:5px 20px;
        border:1px solid rgba(0,0,0,0.4);
        text-shadow:0 -1px 0 rgba(0,0,0,0.4);
        box-shadow:
                inset 0 1px 0 rgba(255,255,255,0.3),
                inset 0 10px 10px rgba(255,255,255,0.1);
        border-radius:0.3em;
        background:#0184ff;
        color:white;
        margin-left: 85px;
        font-weight:bold;
        cursor:pointer;
        font-size:13px;
    }

    input[type=submit]:hover {
        box-shadow:
                inset 0 1px 0 rgba(255,255,255,0.3),
                inset 0 -10px 10px rgba(255,255,255,0.1);
    }

    input[type=text]:hover,
    input[type=password]:hover,
    label:hover ~ input[type=text],
    label:hover ~ input[type=password] {
        background:#27292c;
    }

</style>

<body style="background-image: url(adm/assets/img/car.jpg); background-repeat: no-repeat; background-size: cover; background-position: center;   background-attachment: fixed;">

<form role="form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
    <h1>Log in</h1>
    <div class="inset">
        <p>
            <label for="email">UserID</label>
            <input type="text" name="username" id="username">
        </p>
        <p>
            <label for="password">Password</label>
            <input type="password" name="password" id="inputPassword">
        </p>
    </div>
    <p class="p-container">
        <input type="submit" name="go" id="go" value="Log in">
    </p>
</form>




<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
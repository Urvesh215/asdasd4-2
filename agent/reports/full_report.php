<?php
session_start();
require_once '../../api_info.php';

##$response = http_get($api_server."FullReport?user=".$_SESSION['username']);
$ch = curl_init($api_server."FullReport?user=".$_SESSION['username']);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HEADER, 0);
$result = json_decode(curl_exec($ch), true);

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Bwyn 138</title>
    <!-- Font Awesome -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Sidebar CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/sidebar.css">
    <!-- Themify-icons CSS -->
    <link href="assets/css/themify-icons.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="assets/css/style.css" rel="stylesheet">
</head>

<body onload="startTime()">


<!-- sidebar -->
              <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
            <div class="nano">
                <div class="nano-content">
                    <div id="clock"></div>
                    <p id="date"></p>
                    <div class="divbtn"><button class="btn1" ">Logout</button></div>
                    <div class="logo"><a href="#">LOGO</a></div>
                    <ul>
<?php require_once '../_sidebar/sidebar.php'; ?>

                        
                    </ul>
                </div>
            </div>
        </div>
<!-- /# sidebar -->



<div id="d1">
<marquee style="color: white; font-size: 17px;" behavior="scroll" direction="left">4D Bet closes at  6:15pm on Draw days  ****** Mobile Access : wap.vegas128.com ******  Fixed Bets will not be allow to edit/delete on draw days after 3:50pm.   Thank you for the support ! ! !  Goodluck ! ! ! </marquee>
</div>


<div id="main">
<div>
    <div class="col-md-5">
        <h3 class="page_title">Full Report</h3><br>
    </div>
    <div class="col-md-8 responsive_side_align">
        <form id="search-form" onclick="return false" action="" class="form-inline" style="display: inline;">
            <label style="display: inline;">User ID/Name</label>
            <input type="text" class="form-control form-control-sm btn-sm filter" id="search-filter">
            <button id="find" type="submit" class="btn3">Find</button>
        </form>

        <button id="clear" class="btn3" style="display: inline;">All</button>
        <select style="width: 150px;" name="cars">
          <option value="Account">Account</option>
          <option value="Account">Account</option>
          <option value="Account">Account</option>
          <option value="Account">Account</option>
        </select>
        <label style="display: inline;">From</label>
         <input type="date" name="date">
         <label style="display: inline;">To</label>
         <input type="date" name="date">
        <button class="btn1">View</button>
    </div>
</div><br>


<div class="col-md-12">
<h3>/<?php echo $_SESSION['username']; ?>/ Account For / 23-04-2018 /</h3>
<button style="float: right;" class="btn1">Print Friendly</button><br>
</div>
<br>

<div class="col-md-12">
    <div class="col-md-12 pl-0">
    <table id="stakeholders" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
            <tr style="background-color: #728b85;">
                <th>Account</th>
                <th>Big</th>
                <th>Small</th>
                <th>Amount</th>
                <th>Rebate</th>
                <th>Less Rebate</th>
                <th>Strike</th>
                <th>Net</th>
                <th>Stake(%)</th>
                <th>Stake</th>
                <th>Sub(%)</th>
                <th>Sub Stake</th>
                <th>Net Bal</th>
            </tr>
        <tr>
        <?php
        $i = 0;

        foreach($result as $k => $jsons)
        {

            echo "<td>".$jsons['account']."</td>";
            echo "<td>".($jsons['big'])."</td>";
            echo "<td>".($jsons['small'])."</td>";
            echo "<td>".($jsons['amount'])."</td>";
            echo "<td>".($jsons['rebate'])."</td>";
            echo "<td>".($jsons['less_rebate'])."</td>";
            echo "<td>".($jsons['strike'])."</td>";
            echo "<td>".($jsons['net'])."</td>";
            echo "<td>".($jsons['stake_pcnt'])."</td>";
            echo "<td>".($jsons['stake'])."</td>";
            echo "<td>0.00</td>";
            echo "<td>0.00</td>";
            echo "<td>".($jsons['net'])."</td>";

        }?>
            </tr>
            <tr>

            </tr>
    </table>
    </div>
</div><br>
<div class="col-md-12">

</div>
<div class="col-md-12">
<div class="col-md-12 pl-0">
    <table  id="stakeholders" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
        <tr style="background-color: #728b85;">
            <th style="text-align: center;">Expenses</th>
            <th></th>
        </tr>
        <tr>
            <td style="text-align: center;">Total</td>
            <td>600.00</td>
        </tr>
    </table>
</div><br>
</div>
<div class="col-md-12">
<div class="col-md-12 pl-0">
    <table id="stakeholders" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
        <tr>
            <th></th>
            <th>Big</th>
            <th>Small</th>
            <th>Amount</th>
            <th>Rebate</th>
            <th>Less Rebate</th>
            <th>Strike</th>
            <th>Net</th>
        </tr>
        <tr>
            <?php
            $i = 0;

            foreach($result as $k => $jsons)
            {
                echo "<th></th>";
                echo "<th>".($jsons['big'])."</th>";
                echo "<th>".($jsons['small'])."</th>";
                echo "<th>".($jsons['amount'])."</th>";
                echo "<th>".($jsons['rebate'])."</th>";
                echo "<th>".($jsons['less_rebate'])."</th>";
                echo "<th>".($jsons['strike'])."</th>";
                echo "<th>".($jsons['net'])."</th>";


            }?>
        </tr>
    </table>
</div>
</div>
</div>











    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.nanoscroller.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <!-- Sidebar JavaScript -->
    <script type="text/javascript" src="assets/js/sidebar.js"></script>    
    <!-- Costum JS -->
    <script type="text/javascript" src="assets/js/script.js"></script>
</body>

</html>
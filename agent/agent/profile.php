<!DOCTYPE html>
<html lang="en">

<head>

    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Bwyn 138</title>
    <!-- Font Awesome -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Sidebar CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/sidebar.css">
    <!-- Themify-icons CSS -->
    <link href="assets/css/themify-icons.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="assets/css/style.css" rel="stylesheet">
</head>

<body onload="startTime()">


<!-- sidebar -->
              <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
            <div class="nano">
                <div class="nano-content">
                    <div id="clock"></div>
                    <p id="date"></p>
                    <div class="divbtn"><button class="btn1" ">Logout</button></div>
                    <div class="logo"><a href="#">LOGO</a></div>
                    <ul>
<?php require_once '../_sidebar/sidebar.php'; ?>

                        
                    </ul>
                </div>
            </div>
        </div>
<!-- /# sidebar -->



<div id="d1">
<marquee style="color: white; font-size: 17px;" behavior="scroll" direction="left">4D Bet closes at  6:15pm on Draw days  ****** Mobile Access : wap.vegas128.com ******  Fixed Bets will not be allow to edit/delete on draw days after 3:50pm.   Thank you for the support ! ! !  Goodluck ! ! ! </marquee>
</div>


<div id="main">
<div>
<div class="col-md-12">
    <h2>Agent Managment</h2><br>
    <button style="float: right;" class="btn3">Change Password</button>
</div>
</div>
   
<div>
    <div class="col-md-8">
        <table class="table">
            <thead>
                <tr>
                    <th colspan="2">Account Details</th>
                </tr>
            </thead>
            <tbody>
                <tr class="td_60">
                    <td>User ID</td><td>{{$agent->username}}</td>
                </tr>
                <tr>
                    <td>Name</td><td>{{$agent->name}}</td>
                </tr>
                <tr>
                    <td>Credit Limit</td><td>SGD {{$agent->credit_limit}}</td>
                </tr>
                <tr>
                    <td>Account Balance</td><td>Not Implemented</td>
                </tr>
                <tr>
                    <td>Credit Alocated</td><td>Not Implemented</td>
                </tr>
                <tr>
                    <td>Credit Left</td><td>Not Implemented</td>
                </tr>
            </tbody>
        </table>

        <table class="table">
            <thead>
                <tr>
                    <th colspan="2">4D Settings</th>
                </tr>
            </thead>
            <tbody>
                <tr class="td_60">
                    <td>Big Rate</td><td>SGD 1.60</td>
                </tr>
                <tr>
                    <td>Smal Rate</td><td>SGD 0.80</td>
                </tr>
                <tr>
                    <td>Big Ticket Intake</td><td>{{$agent->big_ticket_intake}}</td>
                </tr>
                <tr>
                    <td>Small Ticket Intake</td><td>{{$agent->small_ticket_intake}}</td>
                </tr>
                <tr>
                    <td>Intake Method</td><td>{{$agent->intake_method}}</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td>View Steak Alocation</td>
                </tr>
            </tfoot>
        </table>
        <table class="table">
    <tr>
        <th>Credit limit</th>
        <th>Account Balance</th>
        <th>Credit Left</th>
    </tr>
    <tr>
        <th>200$</th>
        <th>0$</th>
        <th>200$</th>
    </tr>
</table>
    </div>
</div>
</div>











    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.nanoscroller.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <!-- Sidebar JavaScript -->
    <script type="text/javascript" src="assets/js/sidebar.js"></script>    
    <!-- Costum JS -->
    <script type="text/javascript" src="assets/js/script.js"></script>
</body>

</html>
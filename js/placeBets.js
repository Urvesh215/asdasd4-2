function placeBets(userName, parent) {
    let bets = [];
    $('table.table-bets').find('tr')
        .each((i, tableRow) => {
            const betDay = $(tableRow).find('select.bet-day').val();
            const bet4d = $(tableRow).find('input.bet-4d').val();
            const betBig = $(tableRow).find('input.bet-big').val();
            const betSmall = $(tableRow).find('input.bet-small').val();
            const betRoll = $(tableRow).find('select.bet-roll').val();

            if (!_.isEmpty(bet4d) && (!_.isEmpty(betBig) || !_.isEmpty(betSmall))) {
                // ima bet
                const bet = {
                    draw_day: betDay,
                    ticket_number: bet4d,
                    big_amt: betBig,
                    small_amt: betSmall,
                    roll: betRoll === 'R' ? 1 : 0,
                    ibet: betRoll === 'IB' ? 1 : 0,
                    user_name: userName,
                    user_entered: userName,
                    parent_name: parent
                };
                bets.push(bet);
            }

            // console.log('asd:' + data);
        });
    if (!_.isEmpty(bets)) {
        _.forEach(bets, bet => {
            // url: 'http://18.195.247.159:52817/api/PlaceBets',
            $.post('place_bets.php', {bet: bet})
                .always(data => {
                    console.log(data);
                    if (data == '1') {
                        window.alert('You placed successful bet! Good Luck!');
                    } else {
                        window.alert('There was an error processing your bet request.');
                    }
                });
        })
    }
}